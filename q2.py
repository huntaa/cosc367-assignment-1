
from search import *
import math
import heapq


def valid_path(graph_table, head_node):
    row, col, fuel = head_node
    cell = graph_table[row][col]
    if ((cell != 'X') and cell.isalnum()) or (cell == ' '):
        if fuel >= 0:
            return True
    return False


class RoutingGraph(Graph):
    def __init__(self, graph_string):
        self.graph_table = [row.lstrip() for row in graph_string.splitlines()]
        self.start = []
        self.goal_locations = []
        for row_index, row in enumerate(self.graph_table):
            for col_index, cell in enumerate(row):
                if cell.isdigit():
                    self.start.append((row_index, col_index, int(cell)))
                elif cell == 'S':
                    self.start.append((row_index, col_index, math.inf))
                elif cell == 'G':
                    self.goal_locations.append((row_index, col_index))


    def _NESW(self, pos):
        return [(row, col, d) for row, col, d in \
                [(pos[0]-1, pos[1], 'N'), (pos[0], pos[1]+1, 'E'), (pos[0]+1, pos[1], 'S'), (pos[0], pos[1]-1, 'W')]\
                if row > 0 and col > 0]

    def starting_nodes(self):
        return self.start


    def outgoing_arcs(self, tail_node):
        srow, scol, fuel = tail_node
        result = [Arc(tail=tail_node, head=(drow, dcol, fuel-1), action=d, cost=5) for drow, dcol, d in\
                    self._NESW(tail_node)\
                    if valid_path(self.graph_table, (drow, dcol, fuel-1))]
        if (self.graph_table[srow][scol] == 'F') and fuel < 9:
            result.append(Arc(tail=tail_node, head=(srow, scol, 9), action='Fuel up', cost=15))
        return result

    def is_goal(self, node):
        row, col, fuel = node
        if (self.graph_table[row][col] == 'G') and (fuel >= 0):
            return True
        else:
            return False

    def estimated_cost_to_goal(self, node):
        #result = min([abs(goal[0]-node[0])+abs(goal[1]-node[1]) for goal in self.goal_locations])*5
        return 0



class AStarFrontier(Frontier):
    def __init__(self, routing_graph):
        self.container = []                     #should be priority queue to maintain order
        self.routing_graph = routing_graph
        self.expanded = set()
        self.added = 0

    def __next__(self):
        if len(self.container) > 0:
            smallest_path = heapq.heappop(self.container)[2]
        else:
            raise StopIteration
        return smallest_path


    def add(self, path):
        node = path[-1].head
        if node not in self.expanded:
            self.expanded.add(node)
            path_cost = sum(arc.cost for arc in path)
            heuristic = self.routing_graph.estimated_cost_to_goal(node)
            cost = path_cost + heuristic
            heapq.heappush(self.container, (cost, self.added, path))
            self.added += 1

    def __iter__(self):
        return self

def main():
    map_str = """\
    +----------------+
    |2              F|
    |XX     G 123    |
    |3XXXXXXXXXXXXXX |
    |  F             |
    |          F     |
    +----------------+
    """

    map_graph = RoutingGraph(map_str)
    frontier = AStarFrontier(map_graph)
    solution = next(generic_search(map_graph, frontier), None)
    print_actions(solution)

if __name__ == "__main__":
    main()
