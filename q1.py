from search import *
import math


def valid_path(graph_table, head_node):
    row, col, fuel = head_node
    cell = graph_table[row][col]
    if ((cell != 'X') and cell.isalnum()) or (cell == ' '):
        if fuel >= 0:
            return True
    return False


class RoutingGraph(Graph):
    def __init__(self, graph_string):
        self.graph_table = [row.lstrip() for row in graph_string.splitlines()]
        self.start = []
        for row_index, row in enumerate(self.graph_table):
            for col_index, cell in enumerate(row):
                #print(row_index, col_index, cell)
                if cell.isdigit():
                    self.start.append((row_index, col_index, int(cell)))
                elif cell == 'S': 
                    self.start.append((row_index, col_index, math.inf))


    def _NESW(self, pos):
        return [(row, col, d) for row, col, d in \
                [(pos[0]-1, pos[1], 'N'), (pos[0], pos[1]+1, 'E'), (pos[0]+1, pos[1], 'S'), (pos[0], pos[1]-1, 'W')]\
                if row > 0 and col > 0]
    
    def starting_nodes(self):
        return self.start


    def outgoing_arcs(self, tail_node):
        srow, scol, fuel = tail_node
        result = [Arc(tail=tail_node, head=(drow, dcol, fuel-1), action=d, cost=5) for drow, dcol, d in\
                    self._NESW(tail_node)\
                    if valid_path(self.graph_table, (drow, dcol, fuel-1))]
        if (self.graph_table[srow][scol] == 'F') and fuel < 9:
            result.append(Arc(tail=tail_node, head=(srow, scol, 9), action='Fuel up', cost=15))
        return result

    def is_goal(self, node):
        row, col, fuel = node
        if (self.graph_table[row][col] == 'G') and (fuel >= 0):
            return True
        else:
            return False

def main():
    map_str = """\
    +------+
    |S    S|
    |  GXXX|
    |S     |
    +------+
    """

    graph = RoutingGraph(map_str)
    print("Starting nodes:", sorted(graph.starting_nodes()))
    
if __name__ == "__main__":
    main()
